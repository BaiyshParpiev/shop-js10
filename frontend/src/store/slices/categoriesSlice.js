import {createSlice} from "@reduxjs/toolkit";

const name = 'categories'

const categoriesSlice = createSlice({
  name,
  initialState: {
    categories: [],
    fetchLoadingCategories: false,
  },
  reducers: {
    fetchCategoriesRequest(state, action) {
      state.fetchLoadingCategories = true;
    },
    fetchCategoriesSuccess(state, {payload: categories}) {
      state.categories = categories;
      state.fetchLoadingCategories = false;
    },
    fetchCategoriesFailure(state, action) {
      state.fetchLoadingCategories = false;
    }
  }
});

export default categoriesSlice;