import React from 'react';
import {makeStyles} from "@material-ui/core";
import AppDrawer from "../AppDrawer/AppDrawer";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1
  },
}));

const ProductsLayout = ({children}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppDrawer/>
      <div className={classes.content}>
        {children}
      </div>
    </div>
  );
};

export default ProductsLayout;