import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Grid, TextField} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2)
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const ProductForm = ({onSubmit, categories, error, loading}) => {
  const classes = useStyles();

  const [state, setState] = useState({
    title: "",
    price: "",
    description: "",
    image: null,
    category: "",
  });

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();
    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    onSubmit(formData);
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];
    setState(prevState => {
      return {...prevState, [name]: file};
    });
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  return (
    <Grid
      container
      direction="column"
      spacing={2}
      component="form"
      className={classes.root}
      autoComplete="off"
      onSubmit={submitFormHandler}
      noValidate
    >
      <FormElement
        required
        select
        options={categories}
        label="Category"
        name="category"
        value={state.category}
        onChange={inputChangeHandler}
        error={getFieldError('category')}
      />

      <FormElement
        required
        label="Title"
        name="title"
        value={state.title}
        onChange={inputChangeHandler}
        error={getFieldError('title')}
      />

      <FormElement
        required
        type="number"
        label="Price"
        name="price"
        value={state.price}
        onChange={inputChangeHandler}
        error={getFieldError('price')}
      />

      <FormElement
        multiline
        rows={4}
        label="Description"
        name="description"
        value={state.description}
        onChange={inputChangeHandler}
        error={getFieldError('description')}
      />

      <Grid item xs>
        <TextField
          type="file"
          name="image"
          onChange={fileChangeHandler}
          error={Boolean(getFieldError('image'))}
          helperText={getFieldError('image')}
        />
      </Grid>

      <Grid item xs={12}>
        <ButtonWithProgress
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          loading={loading}
          disabled={loading}
        >
          Create
        </ButtonWithProgress>
      </Grid>
    </Grid>
  );
};

export default ProductForm;