import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import ProductForm from "../../components/ProductForm/ProductForm";
import {createProduct} from "../../store/actions/productsActions";
import {fetchCategoriesRequest} from "../../store/actions/categoriesActions";
import {Typography} from "@material-ui/core";

const NewProduct = () => {
  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories.categories);
  const error = useSelector(state => state.products.createProductError);
  const loading = useSelector(state => state.products.createProductLoading);

  useEffect(() => {
    dispatch(fetchCategoriesRequest());
  }, [dispatch]);

  const onSubmit = productData => {
    dispatch(createProduct(productData));
  };

  return (
    <>
      <Typography variant="h4">New product</Typography>
      <ProductForm
        onSubmit={onSubmit}
        categories={categories}
        error={error}
        loading={loading}
      />
    </>
  );
};

export default NewProduct;